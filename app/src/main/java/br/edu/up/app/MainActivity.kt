package br.edu.up.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun aoClicarNoBotao(view: View) {
        //Vai executar aqui...
        var caixaDeTexto: TextView = findViewById(R.id.textView)
        caixaDeTexto.text = "Olá mundo!"
    }
}